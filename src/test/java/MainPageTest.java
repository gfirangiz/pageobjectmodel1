import Components.MainHeaderComponent;
import Driver.DriverFactory;
import Exceptions.DriverNotSupportedException;
import Pages.MainPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MainPageTest {
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkChooseConcert() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseConcert()
                .pageHeaderShouldBeSameAs("Концерт");
    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
