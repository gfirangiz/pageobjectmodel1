package Driver;

import Driver.impl.ChromeWebDriver;
import Driver.impl.EdgeWebDriver;
import Driver.impl.FirefoxWebDriver;
import Exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Locale;

public class DriverFactory implements IDriverFactory {
    private String browserType = System.getProperty("browser").toLowerCase(Locale.ROOT);


    @Override
    public WebDriver getDriver() throws DriverNotSupportedException {
        switch (this.browserType) {
            case "chrome": {
                return new EventFiringWebDriver(new ChromeWebDriver().newDriver());
            }
            case "firefox": {
                return new EventFiringWebDriver(new FirefoxWebDriver().newDriver());
            }
            case "edge": {
                return new EventFiringWebDriver(new EdgeWebDriver().newDriver());
            }
            default:
                throw new DriverNotSupportedException(this.browserType);
        }

    }

}
